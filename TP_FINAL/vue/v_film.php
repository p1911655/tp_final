<html>
     <head><title>Liste film</title>
     <link href="vue/style.css" rel="stylesheet">
    </head>
<body>
<header>
<p id="titre">LISTE DE FILMS</p>
</header>
<div class="menu">
     <nav>
          <ul class="itemMenu">
               <li><a href="index.php?page=film">ACCEUIL</a></li>
               <li><a href="index.php?page=inscription">INSCRIPTION</a></li>
               <li><a href="index.php?page=connexion">CONNEXION</a></li>
               <li><a href="index.php?page=ajout">AJOUTER FILM</a></li>
          </ul>
     </nav>
</div>
<table>
<thead>
     <?php foreach ($listColumn as $key=>$row) { ?>
   <th> <?php echo $row; ?>
        <a href="index.php?orderBy=<?php echo $row; ?>&orderDirection=ASC"><img src="image/up.jfif" alt="fleche vers le haut" heigth="10" width="10"></a>
        <a href="index.php?orderBy=<?php echo $row; ?>&orderDirection=DESC"><img src="image/down.jfif" alt="fleche vers le bas" heigth="10" width="10"></a>
   </th>
   <?php  } ?>
   </thead>
   <tbody>
        <?php foreach ($listFilm as $key=>$row){ ?>
   <tr>
      <td><?php echo $row["id"]; ?></td>
      <td><?php echo $row["nom"]; ?></td>
      <td><?php echo $row["annee"]; ?></td>
      <td><?php echo $row["score"]; ?></td>
      <td><?php echo $row["nbVotants"]; ?></td>
      <td><a href="index.php?page=suppression&film=<?php echo $row["id"];?>" ><img src="image/corbeille.jfif" alt="Image de suppression" height="15" width="15"></a></td>
   </tr>
   <?php } ?>
   </tbody>
</table>
</body>
</html>
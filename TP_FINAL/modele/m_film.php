<?php
 

     $bdd = connectDb(); //connexion à la BDD
     $queryListColumn = $bdd->prepare('Describe film');
     $queryListColumn->execute();
     $listColumn = $queryListColumn->fetchAll (PDO::FETCH_COLUMN);

     if(isset($_GET["orderBy"])&& isset($_GET["orderDirection"]))
     
     {
          $orderBy = $_GET["orderBy"];
          $orderDirection = $_GET["orderDirection"];
     }else{
          $orderBy = "id";
          $orderDirection = 'ASC';
     }

     $query = $bdd->prepare('select * from film order by ' . $orderBy . ' ' . $orderDirection);
     $query->execute();
     $listFilm = $query->fetchAll();
     $query->closeCursor();

?>